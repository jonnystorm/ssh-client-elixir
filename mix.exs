defmodule SSHClient.MixProject do
  use Mix.Project

  def project do
    [ app: :ssh_client,
      version: "0.2.2",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [
        add_plt_apps: [
          :logger,
          :ssh,
        ],
        ignore_warnings: "dialyzer.ignore",
        flags: [
          :unmatched_returns,
          :error_handling,
          :race_conditions,
          :underspecs,
        ],
      ],
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [ extra_applications: [
        :logger,
        :ssh,
      ],
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [ { :timer_ex,
        git: "https://gitlab.com/jonnystorm/timer-elixir.git"
      },
    ]
  end
end
