# SSHClient

An Elixir wrapper for Erlang SSH client channels

## Installation

Add `ssh_client` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [ { :ssh_client,
      git: "https://gitlab.com/jonnystorm/ssh-client-elixir.git"
    },
  ]
end
```

