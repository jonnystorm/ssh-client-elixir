defmodule SSHClientCheck do
  @moduledoc false

  # Give dialyzer an excuse to analyze the API contracts.
  #
  def scenario1 do
    {:ok, conn} =
      "ssh://test.example.com:2222"
      |> URI.parse
      |> SSHClient.connect(%{username: "test"}, 100)

    {:ok, {0, %{stdout: _}}} =
      SSHClient.execute(conn, "\n", 100)

    :ok = SSHClient.disconnect(conn)
  end

  def scenario2 do
    {:ok, conn} =
      "ssh://test.example.com:2222"
      |> URI.parse
      |> SSHClient.connect(%{username: "test"}, 100)

    {:ok, pty} = SSHClient.start_pty(conn)

    {:ok, %{stdout: _}} =
      SSHClient.expect(pty, "$", 100)

    {:ok, %{stdout: _}} =
      SSHClient.exchange(pty, "\n", "$", 100)

    :ok = SSHClient.send(pty, "exit\n")
  end
end
