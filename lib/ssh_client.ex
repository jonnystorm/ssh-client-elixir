# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule SSHClient do
  @moduledoc """
  An SSH client API.
  """

  require Logger

  import SSHClient.Utility,
    only: [chan_mod: 0, ptychan_mod: 0]

  defp resolve_hostname(hostname) do
    with {:ok, {_, _, _, _, _, erl_addresses}} <-
           hostname
           |> String.to_charlist
           |> :inet.gethostbyname
    do
      addresses =
        Enum.map(erl_addresses, fn tuple ->
          tuple
          |> Tuple.to_list
          |> :binary.list_to_bin
        end)

      {:ok, addresses}
    end
  end

  defp _connect(
    %{address:    address0,
      port:       port0,
      credential: %{username: username} = credential,
      timeout:    timeout,
    }
  )   when is_binary(address0)
       and byte_size(address0) in [4, 16]
       and (port0 in 1..65535 or is_nil(port0))
       and is_binary(username)
       and is_integer(timeout)
       and timeout > 0
  do
    address =
      address0
      |> :binary.bin_to_list
      |> List.to_tuple

    port = port0 || 22

    _ = Logger.info("Connecting to ssh://#{username}@#{inspect(address0)}:#{port}...")

    erl_username =
      String.to_charlist(username)

    cred_to_arg =
      %{:password     => :password,
      # :rsa_password => :rsa_pass_phrase,
      # :dsa_password => :dsa_pass_phrase,
      }

    args =
      credential
      |> Map.take(Map.keys(cred_to_arg))
      |> Enum.reduce([], fn({k, v}, acc) ->
        if v do
          {cred_to_arg[k], String.to_charlist(v)}
          |> List.wrap
          |> Enum.concat(acc)
        else
          acc
        end
      end)
      |> Enum.concat(
        [ user: erl_username,
          silently_accept_hosts: true,
        ]
      )

    :ssh.connect(address, port, args, timeout)
  end

  @type uri :: URI.t

  @type username :: binary

  @type credential
    :: %{username: username}

  @doc """
  Establish a connection to `uri` with the given credential.
  `timeout` is in milliseconds.

  ## Examples

      iex> uri = URI.parse("ssh://test.example.com:2222")
      iex> cred = %{username: "admin", password: "pass"}
      iex> {:ok, pid} = SSHClient.connect(uri, cred, 10000)
  """
  @spec connect(uri, credential, timeout)
    :: {:ok, :ssh.connection_ref}
     | {:error, term}
  def connect(uri, credential, timeout)

  def connect(
    %{scheme: "ssh", host: host, port: port},
    %{username: username} = credential,
    timeout
  )   when is_binary(host)
       and (port in 1..65535 or is_nil(port))
       and is_binary(username)
       and is_integer(timeout)
       and timeout > 0
  do
    with {:ok, addresses} <-
           resolve_hostname(host)
    do
      address = List.first(addresses)

      %{address:    address,
        port:       port,
        credential: credential,
        timeout:    timeout,
      }
      |> _connect()
    end
  end

  @doc """
  Explicitly close an existing connection.
  """
  @spec disconnect(:ssh.connection_ref)
    :: :ok
  def disconnect(ref),
    do: :ssh.close(ref)

  @type input   :: binary
  @type error   :: non_neg_integer | binary
  @type output  :: map
  @type command :: binary

  @type pos_timeout :: pos_integer

  @doc ~S"""
  Execute a single command over a non-interactive, one-time
  use channel, and return the output as a binary. Lines are
  delimited by newline ("\n").

  When execution completes, an exit code is provided with
  the data. If execution is interrupted by a signal, the
  name of the received signal takes the place of an exit
  code.

  ## Examples

      iex> SSHClient.execute(pid, "ls\n", 2000)
      {:ok, {0, %{stdout: "stuff\n"}}}

      iex> SSHClient.execute(pid, "rm -rf bigdir\n", 60000)
      {:ok, {"HUP", %{stdout: "rm -rf bigdir\r\n"}}}
  """
  @spec execute(:ssh.connection_ref, command, pos_timeout)
    :: {:ok, {error, output}}
     | {:error, :etimedout}
     | {:error, :failure}
     | {:error, :closed}
     | :ignore
  def execute(pid, command, timeout)
      when is_pid(pid)
       and is_binary(command)
       and is_integer(timeout)
       and timeout > 0
  do
    with {:ok, channel} <- chan_mod().start_link(pid),
      do: chan_mod().exec(channel, command, timeout)
  end

  @type pty_channel :: pid

  @doc """
  Start a pseudoterminal (PTY), returning an interactive
  channel for use with `send/2`, `expect/3`, and
  `exchange/4`.
  """
  @spec start_pty(:ssh.connection_ref)
    :: {:ok, pty_channel}
     | {:error, :etimedout}
     | :ignore
  def start_pty(pid),
    do: ptychan_mod().start_link(pid)

  @type pattern
    :: Regex.t
     | binary

  @type data :: binary

  @doc """
  Await output matching `pattern` on an interactive PTY
  channel, and return the resulting buffer. On timeout, this
  function returns the unmatched buffer contents. Lines will
  be delimited by carriage-return with newline ("\r\n").

  This is most useful for obtaining the initial banner or
  prompt immediately after issuing `start_pty/1`.
  Afterwards, the preferred method of interaction should be
  `exchange/4`.

  ## Examples

      iex> SSHClient.expect(pty, ~r/(>|#)\s*$/, 5000)
      {:ok, %{stdout: "\r\nprompt>"}}

      iex> SSHClient.expect(pty, "#", 2000)
      {:error, {:etimedout, %{stdout: "stuff\r\n"}}}
  """
  @spec expect(pty_channel, pattern, pos_timeout)
    :: {:ok, output}
     | {:error, :etimedout | :closed}
  defdelegate expect(pid, pattern, timeout),
    to: ptychan_mod()

  @doc """
  Send binary input over an interactive (PTY) channel.
  """
  @spec send(pty_channel, data)
    :: :ok
     | {:error, :closed}
  defdelegate send(pid, data),
    to: ptychan_mod()

  @doc """
  Send and receive binary data over an interactive (PTY)
  channel in a single operation.

  ## Examples

      iex> SSHClient.exchange(pty, "sho ver\n", "#", 2000)
      {:ok, %{stdout: "lots\r\nof\r\noutput\r\n"}}
  """
  @spec exchange(pty_channel, data, pattern, pos_timeout)
    :: {:ok, output}
     | {:error, :etimedout | :closed}
  def exchange(pid, data, pattern, timeout) do
    with :ok <- __MODULE__.send(pid, data),
      do: expect(pid, pattern, timeout)
  end
end
