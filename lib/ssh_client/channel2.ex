# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule SSHClient.Channel2 do
  @moduledoc false
  @behaviour :ssh_client_channel

  require Logger

  defp try_fulfill_request(
    %{request: from} = state
  ) do
    error = state[:error]
    data  = state[:data]

    reply = {:ok, {error, data}}

    cond do
      data != nil and error != nil ->
        :ssh_client_channel.reply(from, reply)

        state
        |> Map.delete(:request)
        |> Map.delete(:error)
        |> Map.delete(:data)

      true ->
        state
    end
  end

  defp try_fulfill_request(state),
    do: state

  ###################### Data events #######################

  @impl :ssh_client_channel
  def handle_ssh_msg(
    {:ssh_cm, ref, {:data, cid, dev, data}},
    %{ref: ref, cid: cid} = state
  ) do
    key = %{0 => :stdout, 1 => :stderr}[dev]

    _ = Logger.debug("Received data from #{key} on channel #{cid}")

    next_state =
      Map.update(state, :data, %{key => data}, fn map ->
        Map.update(map, key, data, & &1 <> data)
      end)

    {:ok, next_state}
  end

  def handle_ssh_msg(
    {:ssh_cm, ref, {:eof, cid}},
    %{ref: ref, cid: cid} = state
  ) do
    _ = Logger.info("Received EOF on channel #{cid}")

    next_state = try_fulfill_request(state)

    {:ok, next_state}
  end

  ##################### Status events ######################

  def handle_ssh_msg(
    { :ssh_cm, ref,
      {:exit_signal, cid, erl_signal, _error, _lang_string}
    },
    %{ref: ref, cid: cid} = state
  ) do
    signal = :binary.list_to_bin(erl_signal)

    next_state =
      state
      |> Map.put(:error, signal)
      |> try_fulfill_request

    {:ok, next_state}
  end

  def handle_ssh_msg(
    {:ssh_cm, ref, {:exit_status, cid, exit_status}},
    %{ref: ref, cid: cid} = state
  ) do
    next_state =
      state
      |> Map.put(:error, exit_status)
      |> try_fulfill_request

    {:ok, next_state}
  end

  def handle_ssh_msg(
    {:ssh_cm, ref, {:closed, cid}},
    %{ref: ref, cid: cid} = state
  ),
    do: {:ok, state}

  @impl :ssh_client_channel
  def handle_msg(
    {:ssh_client_channel_up, cid, ref},
    state
  ) do
    _ = Logger.info("Handling connection #{inspect(ref)}, channel #{cid}")

    next_state =
      state
      |> Map.put(:ref, ref)
      |> Map.put(:cid, cid)

    {:ok, next_state}
  end

  def handle_msg(
    :timeout,
    %{request: from} = state
  ) do
    reply = {:error, :etimedout}

    :ssh_client_channel.reply(from, reply)

    next_state = Map.delete(state, :request)

    {:noreply, next_state}
  end

  @impl :ssh_client_channel
  def handle_call(
    {:exec, %{command: command, timeout: timeout}},
    from,
    %{ref: ref, cid: cid} = state
  ) do
    erl_command =
      :binary.bin_to_list(command)

    result =
      :ssh_connection.exec(ref, cid, erl_command, timeout)

    next_state =
      Map.put(state, :request, from)

    case result do
      :success ->
        {:noreply, next_state, timeout}

      :failure ->
        {:reply, {:error, :failure}, state}

      {:error, :timeout} ->
        {:reply, {:error, :etimedout}, state}
    end
  end

  @impl :ssh_client_channel
  def handle_cast(_msg, state),
    do: {:noreply, state}

  @impl :ssh_client_channel
  def code_change(_vsn, state, _extra),
    do: {:ok, state}

  @impl :ssh_client_channel
  def terminate(_reason, state),
    do: try_fulfill_request(state)

  @impl :ssh_client_channel
  def init(_args),
    do: {:ok, %{}}

  ####################### Public API #######################

  @type ssh_client_channel :: pid

  @spec start_link(:ssh.connection_ref)
    :: {:ok, ssh_client_channel}
     | {:error, any}
     | :ignore
  def start_link(ref) do
    with {:ok, cid} <-
           :ssh_connection.session_channel(ref, 5000)
    do
      :ssh_client_channel.start_link(
        ref,
        cid,
        __MODULE__,
        []
      )
    end
  end

  @type input :: binary
  @type error :: non_neg_integer | binary

  @type output
    :: %{}
     | %{stdout: binary}
     | %{stderr: binary}
     | %{stdout: binary, stderr: binary}

  @type command :: binary

  @spec exec(ssh_client_channel, command, timeout)
    :: {:ok, {error, output}}
     | {:error, :etimedout}
     | {:error, :failure}
     | {:error, :closed}
  def exec(pid, command, timeout)
      when is_pid(pid)
       and is_binary(command)
       and is_integer(timeout)
       and timeout > 0
  do
    args = %{command: command, timeout: timeout}

    with {:error, :timeout} <-
           :ssh_client_channel.call(
             pid,
             {:exec, args},
             timeout + 100
           ),
      do: {:error, :etimedout}
  end
end
