defmodule SSHClient.Utility do
  @moduledoc false

  # OTP >= 21 replaces ssh_channel with ssh_client_channel.
  # Select the appropriate module adhering to the apposite
  # behaviour.
  defmacro chan_mod do
    {otp_rel, ""} =
      System.otp_release
      |> Integer.parse

    if otp_rel < 21 do
      quote do SSHClient.Channel end
    else
      quote do SSHClient.Channel2 end
    end
  end

  # OTP >= 21 replaces ssh_channel with ssh_client_channel.
  # Select the appropriate module adhering to the apposite
  # behaviour.
  defmacro ptychan_mod do
    {otp_rel, ""} =
      System.otp_release
      |> Integer.parse

    if otp_rel < 21 do
      quote do SSHClient.PTYChannel end
    else
      quote do SSHClient.PTYChannel2 end
    end
  end
end
