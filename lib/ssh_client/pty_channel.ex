# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule SSHClient.PTYChannel do
  @moduledoc false
  @behaviour :ssh_channel

  require Logger

  ###################### Data events #######################

  @impl :ssh_channel
  def handle_ssh_msg(
    {:ssh_cm, ref, {:data, cid, dev, data}},
    %{ref: ref, cid: cid} = state
  ) do
    next_state = expect_one(state, {dev, data})

    {:ok, next_state}
  end

  def handle_ssh_msg(
    {:ssh_cm, ref, {:eof, cid}},
    %{ref: ref, cid: cid, request: req} = state
  ) do
    buf =
      state.queue
      |> :queue.to_list
      |> Enum.reduce(req.acc, &accrete/2)

    if Timer.timed_out?(req.timer) do
      msg = {:error, {:etimedout, buf}}

      :ssh_client_channel.reply(req.from, msg)
    else
      :ssh_client_channel.reply(req.from, {:ok, buf})
    end

    {:ok, state}
  end

  def handle_ssh_msg(
    {:ssh_cm, ref, {:eof, cid}},
    %{ref: ref, cid: cid} = state
  ) do
    {:ok, state}
  end

  ##################### Status events ######################

  def handle_ssh_msg(
    { :ssh_cm, ref,
      {:exit_signal, cid, _erl_signal, _error, _lang_string}
    },
    %{ref: ref, cid: cid} = state
  ),
    do: {:ok, state}

  def handle_ssh_msg(
    {:ssh_cm, ref, {:exit_status, cid, _exit_status}},
    %{ref: ref, cid: cid} = state
  ),
    do: {:ok, state}

  def handle_ssh_msg(
    {:ssh_cm, ref, {:closed, cid}},
    %{ref: ref, cid: cid} = state
  ),
    do: {:ok, state}

  @impl :ssh_channel
  def handle_msg(
    {:ssh_channel_up, cid, ref},
    state
  ) do
    _ = Logger.info("Handling connection #{inspect(ref)}, channel #{cid}")

    with :success <-
           :ssh_connection.ptty_alloc(ref, cid, []),

         :ok <- :ssh_connection.shell(ref, cid)
    do
      next_state =
        state
        |> Map.put(:ref, ref)
        |> Map.put(:cid, cid)

      {:ok, next_state}
    else
      error ->
        {:stop, error, nil}
    end
  end

  def handle_msg(:timeout, state) do
    # Clear the expect request state if no messages were
    # received before the timeout from `handle_call/3`.
    #
    next_state = expect_timeout(state)

    {:ok, next_state}
  end

  defp enqueue(state, {dev, data}) do
    if :queue.len(state.queue) < state.max_qlen do
      %{state |
        queue: :queue.in({dev, data}, state.queue),
      }
    else
      %{state|tail_drops: state.tail_drops + 1}
    end
  end

  defp accrete({dev, data}, acc) do
    # Scrub ANSI escape sequences from terminal output
    #
    scrubbed =
      data
      |> String.replace(~r"\e\[[0-9;]*[a-z]"i, "")
      |> String.replace(~r"\e\]0;.*\a", "")  # term title

    key = %{0 => :stdout, 1 => :stderr}[dev]

    Map.update(acc, key, scrubbed, & &1 <> scrubbed)
  end

  defp expect_timeout(%{request: req} = state) do
    msg = {:error, {:etimedout, req.acc}}

    :ssh_client_channel.reply(req.from, msg)

    Map.delete(state, :request)
  end

  defp expect_timeout(state),
    do: state

  defp expect_timeout(state, {dev, data}) do
    state
    |> expect_timeout
    |> enqueue({dev, data})
  end

  defp expect_one(
    %{request: req} = state,
    {dev, data}
  ) do
    # This is all something of a farce because we're
    # expecting the given pattern to occur within a single
    # data message. Instead, we should probably
    #
    #     (req.acc.stdout <> data) =~ req.pattern
    #
    # which becomes more absurd with each recursion.
    #
    next_acc = accrete({dev, data}, req.acc)

    cond do
      Timer.timed_out?(req.timer) ->
        expect_timeout(state, {dev, data})

      data =~ req.pattern ->
        msg = {:ok, next_acc}

        :ssh_client_channel.reply(req.from, msg)

        Map.delete(state, :request)

      true ->
        %{state |
          request: %{req|acc: next_acc},
        }
    end
  end

  defp expect_one(state, {dev, data}),
    do: enqueue(state, {dev, data})

  defp _expect(%{request: _} = state, queue) do
    case :queue.out(queue) do
      {:empty, _} ->
        state

      {{_, v}, q} ->
        state
        |> expect_one(v)
        |> _expect(q)
    end
  end

  defp _expect(state, queue),
    do: %{state|queue: queue}

  defp expect(state) do
    _expect(%{state|queue: :queue.new}, state.queue)
  end

  @impl :ssh_channel
  def handle_call(
    {:expect, pattern, timer},
    from,
    state
  ) do
    req =
      %{pattern: pattern,
        timer:   timer,
        from:    from,
        acc:     %{},
      }

    next_state =
      state
      |> Map.put(:request, req)
      |> expect

    {:noreply, next_state, Timer.remaining(timer)}
  end

  def handle_call({:send, data}, _from, state) do
    result =
      :ssh_connection.send(state.ref, state.cid, data)

    {:reply, result, state}
  end

  @impl :ssh_channel
  def handle_cast(_msg, state),
    do: {:noreply, state}

  @impl :ssh_channel
  def code_change(_vsn, state, _extra),
    do: {:ok, state}

  @impl :ssh_channel
  def terminate(_reason, _state),
    do: nil

  @impl :ssh_channel
  def init(_args) do
    state =
      %{queue: :queue.new,
        max_qlen: 50000,
        tail_drops: 0,
      }

    {:ok, state}
  end

  ####################### Public API #######################

  @type ssh_channel :: pid

  @spec start_link(:ssh.connection_ref)
    :: {:ok, ssh_channel}
     | {:error, any}
     | :ignore
  def start_link(ref) do
    with {:ok, cid} <-
           :ssh_connection.session_channel(ref, 5000)
    do
      :ssh_client_channel.start_link(ref, cid, __MODULE__, [])
    end
  end

  @type pattern
    :: Regex.t
     | binary

  @type pos_timeout :: pos_integer

  @type output :: map

  @spec expect(ssh_channel, pattern, pos_timeout)
    :: {:ok, output}
     | {:error, :etimedout | :closed}
  def expect(pid, pattern, timeout)
      when is_pid(pid)
       and (is_binary(pattern) or is_map(pattern))
       and is_integer(timeout)
       and timeout > 0
  do
    timer =
      Timer.new(timeout: timeout)
      |> Timer.start

    msg = {:expect, pattern, timer}

    call_timeout = timeout + 100

    if Regex.regex?(pattern)
       or is_binary(pattern)
    do
      :ssh_client_channel.call(pid, msg, call_timeout)
    else
      {:error, :einval}
    end
  end

  @type data :: binary

  @spec send(ssh_channel, data)
    :: :ok
     | {:error, :closed}
  def send(pid, data)
      when is_pid(pid)
       and is_binary(data)
  do
    :ssh_client_channel.call(pid, {:send, data})
  end
end
