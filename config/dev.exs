use Mix.Config

config :logger, level: :info

config :logger, :console,
  metadata: [:module, :function, :pid]

